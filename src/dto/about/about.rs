use dto;

#[derive(Debug, Clone, Default, Serialize)]
pub struct About {
    pub app_name:               &'static str,
    pub run_user:               String,
    pub available_transactions: Vec<String>
}

impl dto::ResponseTrait for About {
    fn name() -> &'static str {
        "about"
    }
}