use std::fmt;

#[derive(Debug, Serialize)]
pub struct Day {
    pub date:           String,
    pub worker_name:    String,
    pub shift:          String,
    pub position:       String,
}

impl Day {
    pub fn new(date: String, worker_name: String, shift: String, position: String) -> Self {
        Self {
            date,
            worker_name,
            shift,
            position
        }
    }
}

impl fmt::Display for Day {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "|{}|{}|{}|{}|", self.date, self.worker_name, self.shift, self.position)
    }
}