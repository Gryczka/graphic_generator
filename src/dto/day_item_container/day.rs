use dto::{Worker, Shift, Position};
use chrono::NaiveDate;

use std::collections::{BTreeSet, BTreeMap};

#[derive(Debug, Clone)]
pub struct Day<'a> {
    pub date:   NaiveDate,
    pub workers_shifts: BTreeMap<String, (&'a Shift, &'a Position)>, // worker, shift, position
    pub positions_shifts: BTreeSet<(&'a String, &'a String)> // shift, position
    // pub shifts: Vec<&'a Shift>,
    // pub positions: Vec<&'a Positions>
}

impl<'a> Day<'a> {
    pub fn new(date: NaiveDate) -> Self {
        Day {
            date,
            workers_shifts: BTreeMap::new(),
            positions_shifts: BTreeSet::new(),
        }
    }

    pub fn upsert_shift(&mut self, worker: &Worker, shift: &'a Shift, position: &'a Position) {
        self.workers_shifts.insert(worker.name.clone(), (shift, position));
        self.positions_shifts.insert((&shift.code, &position.name));
        // self.shifts.push(shift);
        // self.positions.push(position);
    }

    pub fn is_worker(&mut self, worker: &Worker) -> bool {
        self.workers_shifts.get(&worker.name).is_some()
    }

    pub fn is_covered(&self, shifts: &Vec<Shift>, positions: &Vec<&'a Position>) -> bool {
        for shift_expected in shifts {
            for position_expected in positions {
                let mut is_covered = false;
                for &(shift, position) in &self.positions_shifts {
                    if *shift == shift_expected.code && *position == position_expected.name {
                        is_covered = true;
                    }
                }
                if !is_covered {
                    return false;
                }
            }
        }
        true
    }
}