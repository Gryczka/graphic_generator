use dto::day_item_container::Day;
use dto::{Position, Worker, Shift, Day as dtoDay};
use graphic_informations::GraphicInformationsDoc;
use chrono::{Datelike, Duration, NaiveDate, Weekday};
use rand;
use rand::distributions::{IndependentSample, Range};

use std::collections::BTreeMap;

#[derive(Debug)]
pub struct DayContainer<'a> {
    hours: i32,
    days: Vec<Day<'a>>
}

impl<'a> DayContainer<'a> {
    pub fn new(date: &String, shifts: &Vec<Shift>) -> Self {
        let date = NaiveDate::parse_from_str(format!("{}01", date).as_str(), "%Y%m%d").unwrap();
        let mut days = Vec::new();
        let mut hours = 0;

        for offset in 0..31 {
            let date_with_offset = date+Duration::days(offset);
            if (date_with_offset).month() != date.month() {
                break;
            }
            if date_with_offset.weekday() != Weekday::Sat && date_with_offset.weekday() != Weekday::Sun {
                hours += shifts[0].duration as i32;
            }
            days.push(Day::new(date_with_offset));
        }

        DayContainer {
            hours: hours,
            days: days
        }
    }

    pub fn get_days(&self) -> Vec<dtoDay> {
        let mut retval = Vec::new();
        for day in &self.days {
            for (worker, &(shift, position)) in &day.workers_shifts {
                retval.push(dtoDay::new(day.date.to_string(), worker.clone(), shift.code.clone(), position.name.clone()));
            }
        }
        retval
    }

    pub fn generate_graphic_for_one_group(&mut self, graphic_info: &'a GraphicInformationsDoc, positions: Vec<&'a Position>, workers: Vec<&'a Worker>) {
        let range = Range::new(0, workers.len());
        let mut rng = rand::thread_rng();
        let mut workers_days: BTreeMap<&String, (i32, i32)> = BTreeMap::new();
        let mut workers_working_days: BTreeMap<&String, i32> = BTreeMap::new();

        for day in &mut self.days {
            for shift in &graphic_info.shifts {
                for position in &positions {
                    let mut worker = workers[range.ind_sample(&mut rng)];
                    while day.is_worker(worker) {
                        worker = workers[range.ind_sample(&mut rng)];
                    }


                    while {
                        let mut temp = false;
                        if let Some(&(worker_days, free_days)) = workers_days.get(&worker.name) {
                            if worker_days > 3 {
                                temp = true;
                            }
                        }
                        temp
                    } {
                        worker = workers[range.ind_sample(&mut rng)];
                    }


                    if let Some(&mut (ref mut worker_days, ref mut free_days)) = workers_days.get_mut(&worker.name) {
                        *worker_days += 1;
                        *free_days = 0;
                        day.upsert_shift(worker, shift, position);
                        if let Some(mut working_days) = workers_working_days.get_mut(&worker.name) {
                            *working_days += 8;
                            continue;
                        }
                        workers_working_days.insert(&worker.name, 8);
                        continue;
                    }
                    day.upsert_shift(worker, shift, position);
                    workers_days.insert(&worker.name, (1, 0));
                }
            }
            for worker in &workers {
                if !day.is_worker(worker) {
                    if let Some(&mut (ref mut worker_days, ref mut free_days)) = workers_days.get_mut(&worker.name) {
                        *worker_days = 0;
                        *free_days += 1;
                    }
                    workers_days.insert(&worker.name, (0, 1));
                }
                if let Some(&mut (ref worker_days, ref free_days)) = workers_days.get_mut(&worker.name) {
                    println!("{} {} {} {}", day.date.to_string(), worker.name, worker_days, free_days);
                    continue;
                }
            }
        }
        for (worker, days) in workers_working_days {
            println!("WORKER {} DAY {}", worker, days);
        }
    }
}