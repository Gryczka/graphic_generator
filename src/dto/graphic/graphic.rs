#[derive(Debug, Deserialize)]
pub struct Graphic {
    pub graphic_id: String,
    pub date: String
}