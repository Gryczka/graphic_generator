use std::fmt;

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct GraphicInfo {
    #[serde(rename = "Graphic_id")]
    pub graphic_id: String,
    #[serde(rename = "Name")]
    pub name:       String
}

impl fmt::Display for GraphicInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "name = {}", self.name)?;
        writeln!(f, "graphic_id = {}", self.graphic_id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_string() {
        let target = GraphicInfo { graphic_id: "ID".to_string(), name: "Name".to_string()};
        assert_eq!("name = Name\ngraphic_id = ID\n", target.to_string());
    }
}