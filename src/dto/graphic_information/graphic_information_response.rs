use dto;

#[derive(Debug, Clone, Default, Serialize)]
pub struct GraphicInformationResponse {
    message: String
}

impl GraphicInformationResponse {
    pub fn new(message: String) -> GraphicInformationResponse {
        GraphicInformationResponse {
            message
        }
    }
}

impl dto::ResponseTrait for GraphicInformationResponse {
    fn name() -> &'static str {
        "graphic_information"
    }
}