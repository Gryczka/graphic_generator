use std::fmt;

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Group {
    #[serde(rename = "Code")]
    pub code:               String,
    #[serde(rename = "Name")]
    pub name:               String,
    #[serde(rename = "Count_of_person")]
    pub count_of_person:    u8
}

impl Group {
    pub fn new(code: String, name: String, count_of_person: u8) -> Self {
        Self {
            code,
            name,
            count_of_person
        }
    }
}

impl fmt::Display for Group {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "|{}|{}|{}|", self.code, self.name, self.count_of_person)
    }
}