use std::fmt;

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Position {
    #[serde(rename = "Name")]
    pub name:               String,
    #[serde(rename = "Group")]
    pub group:              String,
    #[serde(rename = "Count_of_person")]
    pub count_of_person:    u8
}

impl Position {
    pub fn new(name: String, group: String, count_of_person: u8) -> Self {
        Self {
            name,
            group,
            count_of_person
        }
    }
}

impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "|{}|{}|{}|", self.name, self.group, self.count_of_person)
    }
}