use rocket::http::Status;

pub trait ResponseTrait {
    fn name() -> &'static str;
    fn status(&self) -> Status {
        Status::Ok
    }
}