use std::fmt;

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
pub struct Shift {
    #[serde(rename = "Code")]
    pub code:           String,
    #[serde(rename = "Description")]
    pub description:    String,
    #[serde(rename = "Duration")]
    pub duration:       u8,
}

impl Shift {
    pub fn new(code: String, description: String, duration: u8) -> Self {
        Self {
            code,
            description,
            duration
        }
    }
}

impl fmt::Display for Shift {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "|{}|{}|{}|", self.code, self.description, self.duration)
    }
}