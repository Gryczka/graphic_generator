use std::fmt;
use dto::{Shift, WorkingTime};
use std::collections::BTreeMap;
use chrono::NaiveDate;

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
pub struct Worker {
    #[serde(rename = "Name")]
    pub name:           String,
    #[serde(rename = "Group")]
    pub group:          String,
    #[serde(rename = "Working_time")]
    pub working_time:   WorkingTime,
    #[serde(rename = "Position")]
    pub position:       Option<String>,
    // #[serde(skip_serializing)]
    // pub day_shift:      BTreeMap<String, Shift>,
    // #[serde(skip_serializing)]
    // pub working_days:   i32,
}

impl Worker {
    pub fn new(name: String, group: String, working_time: f64, position: Option<String>) -> Self {
        Self {
            name,
            group,
            working_time: WorkingTime { time: working_time},
            position,
            // day_shift: BTreeMap::new(),
            // working_days: 0
        }
    }
}

impl fmt::Display for Worker {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "|{}|{}|{}|{}|", self.name, self.group, self.working_time.time, self.position.as_ref().unwrap_or(&String::new()))
    }
}