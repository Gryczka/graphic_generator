#[derive(Debug, Default, Deserialize, Serialize, Clone)]
pub struct WorkingTime {
    #[serde(rename = "Time")]
    pub time: f64
}