use rocket;
use rocket::{Catcher, Error, Request};


pub fn create() -> Vec<Catcher> {
    errors![
        bad_request,
        unauthorized,
        forbidden,
        not_found,
        internal_error
    ]
}

#[error(400)]
fn bad_request(_error: Error, req: &Request) -> String {
    format!("bad_request! original req: '{}'", req)
}

#[error(401)]
fn unauthorized(_error: Error, req: &Request) -> String {
    format!("unauthorized! original req: '{}'", req)
}

#[error(403)]
fn forbidden(_error: Error, req: &Request) -> String {
    format!("forbidden! original req: '{}'", req)
}

#[error(404)]
fn not_found(_error: Error, req: &Request) -> String {
    format!("not_found! original req: '{}'", req)
}

#[error(500)]
fn internal_error(_error: Error, req: &Request) -> String {
    format!("internal_error! original req: '{}'", req)
}
