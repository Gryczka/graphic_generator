pub const VERSION : &'static str = env!("CARGO_PKG_VERSION");
pub const APP_NAME: &'static str = env!("CARGO_PKG_NAME");
pub const DATA_FOLDER_PATH: &'static str = "tests/data";

pub struct ExitCodes;

impl ExitCodes {
    pub const OK: i32                               = 0;
    pub const FAILED_READING_ROCKET_CONFIG: i32     = 1;
}