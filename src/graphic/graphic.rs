use std::fmt;
use graphic_informations::GraphicInformationsDoc;
use dto::Day;
use dto;

#[derive(Debug, Default, Serialize)]
pub struct Graphic {
    #[serde(skip_serializing)]
    pub graphic_info:   GraphicInformationsDoc,
    pub days:           Vec<Day>
}

impl Graphic {
    pub fn new(graphic_info: GraphicInformationsDoc) -> Self {
        Graphic {
            graphic_info,
            days: Vec::new()
        }
    }

    pub fn add_day(&mut self, day: Day) {
        self.days.push(day);
    }
}

impl fmt::Display for Graphic {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}", self.graphic_info)?;

        writeln!(f, "[DAYS]")?;
        for day in &self.days {
            write!(f, "{}", day)?;
        }
        Ok(())
    }
}

impl dto::ResponseTrait for Graphic {
    fn name() -> &'static str {
        "graphic"
    }
}