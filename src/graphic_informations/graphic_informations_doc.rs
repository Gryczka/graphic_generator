use std::fmt;
use dto::{GraphicInfo, Group, Worker, Shift, Position};

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct GraphicInformationsDoc {
    #[serde(rename = "Graphic_info")]
    pub graphic_info:   GraphicInfo,
    #[serde(rename = "Groups")]
    pub groups:         Vec<Group>,
    #[serde(rename = "Shifts")]
    pub shifts:         Vec<Shift>,
    #[serde(rename = "Workers")]
    pub workers:        Vec<Worker>,
    #[serde(rename = "Positions")]
    pub positions:      Vec<Position>
}

impl GraphicInformationsDoc {
    pub fn set_graphic_info_opt(&mut self, graphic_info: GraphicInfo) {
        self.graphic_info = graphic_info;
    }

    pub fn add_group(&mut self, group: Group) {
        self.groups.push(group);
    }

    pub fn add_shift(&mut self, shift: Shift) {
        self.shifts.push(shift);
    }

    pub fn add_worker(&mut self, worker: Worker) {
        self.workers.push(worker);
    }

    pub fn add_position(&mut self, position: Position) {
        self.positions.push(position);
    }
}

impl fmt::Display for GraphicInformationsDoc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "[GRAPHIC.INFO]")?;
        writeln!(f, "{}", self.graphic_info)?;

        writeln!(f, "[GROUPS]")?;
        for group in &self.groups {
            write!(f, "{}", group)?;
        }
        writeln!(f, "")?;
        writeln!(f, "[SHIFTS]")?;
        for shift in &self.shifts {
            write!(f, "{}", shift)?;
        }
        writeln!(f, "")?;
        writeln!(f, "[POSITIONS]")?;
        for position in &self.positions {
            write!(f, "{}", position)?;
        }
        writeln!(f, "")?;
        writeln!(f, "[WORKERS]")?;
        for worker in &self.workers {
            write!(f, "{}", worker)?;
        }
        writeln!(f, "")?;
        Ok(())
    }
}