use std::path::{Path, PathBuf};
use std::io::{BufReader, self};
use std::fs;
use std::fs::{DirEntry, File};
use std::io::Write;

use graphic::Graphic;
use graphic_repository::Reader;
use graphic_informations::GraphicInformationsDoc;


#[derive(Debug)]
pub struct FileRepository {
    graphic_id:         String,
    source_folder_path: PathBuf,
}

impl FileRepository {
    pub fn new<P: AsRef<Path>>(data_folder_path: P, graphic_id: String) -> FileRepository {
        FileRepository {
            source_folder_path: build_source_folder_path(&data_folder_path, &graphic_id),
            graphic_id:         graphic_id,
        }
    }

    pub fn save_graphic(&self, graphic: &Graphic, date: &str) -> Result<(), io::Error> {
        let full_file_path = self.build_graphic_file_path(date);

        fs::create_dir_all(&self.source_folder_path)?;
        let mut file = File::create(&full_file_path)?;
        file.write_fmt(format_args!("{}", graphic))?;
        file.sync_all()?;
        Ok(())
    }

    pub fn save_graphic_info(&self, graphic_informations_doc: &GraphicInformationsDoc) -> Result<(), io::Error> {
        let full_file_path = self.build_graphic_info_file_path();

        fs::create_dir_all(&self.source_folder_path)?;
        let mut file = File::create(&full_file_path)?;
        file.write_fmt(format_args!("{}", graphic_informations_doc))?;
        file.sync_all()?;
        Ok(())
    }

    fn graphic_info_exists(&self) -> bool {
        let full_file_path = self.build_graphic_info_file_path();
        full_file_path.as_path().exists()
    }

    fn graphic_exists(&self, date: &str) -> bool {
        let full_file_path = self.build_graphic_file_path(date);
        full_file_path.as_path().exists()
    }

    pub fn load_graphic_info(&self) -> Result<GraphicInformationsDoc, io::Error> {
        let full_file_path = self.build_graphic_info_file_path();
        validate_path(&full_file_path)?;

        let file = File::open(&full_file_path)?;
        let reader = Reader::new(file);

        reader.read_graphic_informations_doc()
    }

    pub fn load_graphic(&self, date: &str) -> Result<Graphic, io::Error> {
        let full_file_path = self.build_graphic_file_path(date);
        validate_path(&full_file_path)?;

        let file = File::open(&full_file_path)?;
        let reader = Reader::new(file);

        reader.read_graphic()
    }

    pub fn get_source_folder_path(&self) -> &PathBuf {
        &self.source_folder_path
    }

    fn build_graphic_info_file_path(&self) -> PathBuf {
        let mut path = PathBuf::new();
        path.push(&self.source_folder_path);
        path.push(&self.graphic_id);
        path.set_extension("info");
        path
    }

    fn build_graphic_file_path(&self, date: &str) -> PathBuf {
        let mut path = PathBuf::new();
        path.push(&self.source_folder_path);
        path.push(format!("{}_{}", &self.graphic_id, date));
        path.set_extension("graphic");
        path
    }
}

fn validate_path<'a>(full_file_path: &'a PathBuf) -> Result<(), io::Error> {
    if !full_file_path.as_path().exists() {
        return Err(io::Error::new(io::ErrorKind::NotFound, format!("The file path '{:?}' does not exist", full_file_path)));
    }

    Ok(())
}

fn build_source_folder_path<P: AsRef<Path>>(data_folder_path: P, graphic_id: &String) -> PathBuf {
    let mut path = PathBuf::new();
    path.push(data_folder_path);
    path.push(graphic_id);
    path
}

#[cfg(test)]
mod tests {
    use super::*;

    mod build_graphic_info_file_path {
        use super::*;

        #[test]
        fn builds_file_path() {
            let target = FileRepository::new("tests/data", "1".to_string());
            let result = target.build_graphic_info_file_path();

            assert_eq!("tests/data/1/1.info", result.to_str().expect("Build graphic info file path"));
        }
    }

    mod build_graphic_file_path {
        use super::*;

        #[test]
        fn builds_file_path() {
            let target = FileRepository::new("tests/data", "1".to_string());
            let result = target.build_graphic_file_path("201709");

            assert_eq!("tests/data/1/1_201709.graphic", result.to_str().expect("Build graphic file path"));
        }
    }
}