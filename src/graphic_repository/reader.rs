use std::fs::File;
use std::io::{BufRead, BufReader, self};

use dto::{Day, Group, Worker, Shift, Position};
use graphic_informations::GraphicInformationsDoc;
use graphic::Graphic;

const GRAPHIC_INFO: &'static str    = "[GRAPHIC.INFO]";
const GROUPS: &'static str          = "[GROUPS]";
const WORKERS: &'static str         = "[WORKERS]";
const DAYS: &'static str            = "[DAYS]";
const SHIFTS: &'static str          = "[SHIFTS]";
const POSITIONS: &'static str       = "[POSITIONS]";


#[derive(Debug)]
pub struct Reader {
    file: File
}

impl Reader {
    pub fn new(file: File) -> Self {
        Self { file }
    }

    pub fn read_graphic_informations_doc(&self) -> Result<GraphicInformationsDoc, io::Error> {
        let reader = BufReader::new(&self.file);
        let mut section = String::new();

        let mut doc = GraphicInformationsDoc::default();

        for line in reader.lines() {
            let line = line?;
            match line.as_str() {
                GRAPHIC_INFO | GROUPS | WORKERS | SHIFTS | POSITIONS    => section = line,
                _                                                       => {
                    match section.as_str() {
                        GRAPHIC_INFO    => read_graphic_info(&mut doc, line),
                        GROUPS          => read_group(&mut doc, line),
                        SHIFTS          => read_shift(&mut doc, line),
                        POSITIONS       => read_position(&mut doc, line),
                        WORKERS         => read_worker(&mut doc, line),
                        _               => ()
                    }
                }
            }
        }
        Ok(doc)
    }

    pub fn read_graphic(&self) -> Result<Graphic, io::Error> {
        let reader = BufReader::new(&self.file);
        let mut section = String::new();

        let mut graphic = Graphic::default();

        for line in reader.lines() {
            let line = line?;
            match line.as_str() {
                GRAPHIC_INFO | GROUPS | WORKERS | SHIFTS | POSITIONS | DAYS => section = line,
                _                                                           => {
                    match section.as_str() {
                        GRAPHIC_INFO    => read_graphic_info(&mut graphic.graphic_info, line),
                        GROUPS          => read_group(&mut graphic.graphic_info, line),
                        SHIFTS          => read_shift(&mut graphic.graphic_info, line),
                        POSITIONS       => read_position(&mut graphic.graphic_info, line),
                        WORKERS         => read_worker(&mut graphic.graphic_info, line),
                        DAYS            => read_day(&mut graphic, line),
                        _               => ()
                    }
                }
            }
        }
        Ok(graphic)
    }
}

fn read_graphic_info(doc: &mut GraphicInformationsDoc, line: String)  {
    match (line.find(" "), line.rfind(" ")) {
        (Some(space), Some(last))     => {
            match (line.split_at(space).0 , line.split_at(last + 1).1.to_string()) {
                ("name", element)               => doc.graphic_info.name = element,
                ("graphic_id", element)         => doc.graphic_info.graphic_id = element,
                _                               => ()
            }
        },
        (_, _)            => (),
    }
}

fn read_group(doc: &mut GraphicInformationsDoc, line: String) {
    let result = read(line);

    if let Some(code) = result.get(0) {
        if let Some(name) = result.get(1) {
            if let Some(count) = result.get(2) {
                doc.add_group(Group::new(code.to_string(),
                                         name.to_string(),
                                         count.parse::<u8>().unwrap()));
            }
        }
    }
}

fn read_shift(doc: &mut GraphicInformationsDoc, line: String) {
    let result = read(line);

    if let Some(code) = result.get(0) {
        if let Some(description) = result.get(1) {
            if let Some(duration) = result.get(2) {
                doc.add_shift(Shift::new(code.to_string(),
                                         description.to_string(),
                                         duration.parse::<u8>().unwrap()));
            }
        }
    }
}

fn read_position(doc: &mut GraphicInformationsDoc, line: String) {
    let result = read(line);

    if let Some(name) = result.get(0) {
        if let Some(group) = result.get(1) {
            if let Some(count) = result.get(2) {
                doc.add_position(Position::new(name.to_string(),
                                               group.to_string(),
                                               count.parse::<u8>().unwrap()));
            }
        }
    }
}

fn read_worker(doc: &mut GraphicInformationsDoc, line: String) {
    let result = read(line);

    if let Some(name) = result.get(0) {
        if let Some(group) = result.get(1) {
            if let Some(working_time) = result.get(2) {
                doc.add_worker(Worker::new(name.to_string(),
                                           group.to_string(),
                                           working_time.parse::<f64>().unwrap(),
                                           result.get(3).map(|e| e.to_string())));
            }
        }
    }
}

fn read_day(graphic: &mut Graphic, line: String) {
    let result = read(line);

    if let Some(date) = result.get(0) {
        if let Some(worker_name) = result.get(1) {
            if let Some(shift) = result.get(2) {
                if let Some(position) = result.get(3) {
                    graphic.add_day(Day::new(date.to_string(),
                                             worker_name.to_string(),
                                             shift.to_string(),
                                             position.to_string()));
                }
            }
        }
    }
}

fn read<'a>(line: String) -> Vec<String> {
    let mut result = line.split("|").collect::<Vec<_>>();

    result.retain(|&element| element != "");

    result.iter().map(|element| element.to_string()).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_graphic_informations_doc() {
        let target = Reader::new(File::open("tests/data/1/1.info").unwrap());

        let result = target.read_graphic_informations_doc().expect("GraphicInformationsDoc");

        assert_eq!("DDD",   result.graphic_info.name);
        assert_eq!("1",     result.graphic_info.graphic_id);

        assert_eq!(4,               result.groups.len());
            assert_eq!("1",         result.groups[0].code);
            assert_eq!("GroupA",    result.groups[0].name);

            assert_eq!("2",         result.groups[1].code);
            assert_eq!("GroupB",    result.groups[1].name);

            assert_eq!("3",         result.groups[2].code);
            assert_eq!("GroupC",    result.groups[2].name);

            assert_eq!("4",         result.groups[3].code);
            assert_eq!("GroupD",    result.groups[3].name);

        assert_eq!(2,               result.shifts.len());
            assert_eq!("1",         result.shifts[0].code);
            assert_eq!("8-16",      result.shifts[0].description);

            assert_eq!("2",         result.shifts[1].code);
            assert_eq!("16-24",     result.shifts[1].description);

        assert_eq!(8, result.positions.len());
            assert_eq!("mieso",     result.positions[0].name);
            assert_eq!("1",         result.positions[0].group);
            assert_eq!(1,           result.positions[0].count_of_person);

            assert_eq!("makarony",  result.positions[1].name);
            assert_eq!("1",         result.positions[1].group);
            assert_eq!(1,           result.positions[1].count_of_person);

            assert_eq!("wydawka",   result.positions[2].name);
            assert_eq!("1",         result.positions[2].group);
            assert_eq!(1,           result.positions[2].count_of_person);

            assert_eq!("pomoc",     result.positions[3].name);
            assert_eq!("1",         result.positions[3].group);
            assert_eq!(3,           result.positions[3].count_of_person);

            assert_eq!("salatki",   result.positions[4].name);
            assert_eq!("1",         result.positions[4].group);
            assert_eq!(1,           result.positions[4].count_of_person);

            assert_eq!("salaA",     result.positions[5].name);
            assert_eq!("2",         result.positions[5].group);
            assert_eq!(2,           result.positions[5].count_of_person);

            assert_eq!("salaB",     result.positions[6].name);
            assert_eq!("2",         result.positions[6].group);
            assert_eq!(2,           result.positions[6].count_of_person);

            assert_eq!("salaC",     result.positions[7].name);
            assert_eq!("2",         result.positions[7].group);
            assert_eq!(2,           result.positions[7].count_of_person);

        assert_eq!(4,                               result.workers.len());
            assert_eq!("MagdalenaGryczka",          result.workers[0].name);
            assert_eq!("1",                         result.workers[0].group);
            assert_eq!(1.,                          result.workers[0].working_time.time);
            assert_eq!(None,                        result.workers[0].position);

            assert_eq!("MartynaGryczka",            result.workers[1].name);
            assert_eq!("1",                         result.workers[1].group);
            assert_eq!(0.5,                         result.workers[1].working_time.time);
            assert_eq!(None,                        result.workers[1].position);

            assert_eq!("ElżbietaGryczka",           result.workers[2].name);
            assert_eq!("2",                         result.workers[2].group);
            assert_eq!(0.75,                        result.workers[2].working_time.time);
            assert_eq!(None,                        result.workers[2].position);

            assert_eq!("ObiWanKenobi",              result.workers[3].name);
            assert_eq!("1",                         result.workers[3].group);
            assert_eq!(1.,                          result.workers[3].working_time.time);
            assert_eq!(Some("mieso".to_string()),   result.workers[3].position);
    }

    #[test]
    fn read_graphic() {
        let target = Reader::new(File::open("tests/data/1/1.graphic").unwrap());

        let result = target.read_graphic().expect("Graphic");

        assert_eq!("DDD",   result.graphic_info.graphic_info.name);
        assert_eq!("1",     result.graphic_info.graphic_info.graphic_id);

        assert_eq!(4,               result.graphic_info.groups.len());
            assert_eq!("1",         result.graphic_info.groups[0].code);
            assert_eq!("GroupA",    result.graphic_info.groups[0].name);

            assert_eq!("2",         result.graphic_info.groups[1].code);
            assert_eq!("GroupB",    result.graphic_info.groups[1].name);

            assert_eq!("3",         result.graphic_info.groups[2].code);
            assert_eq!("GroupC",    result.graphic_info.groups[2].name);

            assert_eq!("4",         result.graphic_info.groups[3].code);
            assert_eq!("GroupD",    result.graphic_info.groups[3].name);

        assert_eq!(2,               result.graphic_info.shifts.len());
            assert_eq!("1",         result.graphic_info.shifts[0].code);
            assert_eq!("8-16",      result.graphic_info.shifts[0].description);

            assert_eq!("2",         result.graphic_info.shifts[1].code);
            assert_eq!("16-24",     result.graphic_info.shifts[1].description);

        assert_eq!(8,               result.graphic_info.positions.len());
            assert_eq!("mieso",     result.graphic_info.positions[0].name);
            assert_eq!("1",         result.graphic_info.positions[0].group);
            assert_eq!(1,           result.graphic_info.positions[0].count_of_person);

            assert_eq!("makarony",  result.graphic_info.positions[1].name);
            assert_eq!("1",         result.graphic_info.positions[1].group);
            assert_eq!(1,           result.graphic_info.positions[1].count_of_person);

            assert_eq!("wydawka",   result.graphic_info.positions[2].name);
            assert_eq!("1",         result.graphic_info.positions[2].group);
            assert_eq!(1,           result.graphic_info.positions[2].count_of_person);

            assert_eq!("pomoc",     result.graphic_info.positions[3].name);
            assert_eq!("1",         result.graphic_info.positions[3].group);
            assert_eq!(3,           result.graphic_info.positions[3].count_of_person);

            assert_eq!("salatki",   result.graphic_info.positions[4].name);
            assert_eq!("1",         result.graphic_info.positions[4].group);
            assert_eq!(1,           result.graphic_info.positions[4].count_of_person);

            assert_eq!("salaA",     result.graphic_info.positions[5].name);
            assert_eq!("2",         result.graphic_info.positions[5].group);
            assert_eq!(2,           result.graphic_info.positions[5].count_of_person);

            assert_eq!("salaB",     result.graphic_info.positions[6].name);
            assert_eq!("2",         result.graphic_info.positions[6].group);
            assert_eq!(2,           result.graphic_info.positions[6].count_of_person);

            assert_eq!("salaC",     result.graphic_info.positions[7].name);
            assert_eq!("2",         result.graphic_info.positions[7].group);
            assert_eq!(2,           result.graphic_info.positions[7].count_of_person);

        assert_eq!(4,                               result.graphic_info.workers.len());
            assert_eq!("MagdalenaGryczka",          result.graphic_info.workers[0].name);
            assert_eq!("1",                         result.graphic_info.workers[0].group);
            assert_eq!(1.,                          result.graphic_info.workers[0].working_time.time);
            assert_eq!(None,                        result.graphic_info.workers[0].position);

            assert_eq!("MartynaGryczka",            result.graphic_info.workers[1].name);
            assert_eq!("1",                         result.graphic_info.workers[1].group);
            assert_eq!(0.5,                         result.graphic_info.workers[1].working_time.time);
            assert_eq!(None,                        result.graphic_info.workers[1].position);

            assert_eq!("ElżbietaGryczka",           result.graphic_info.workers[2].name);
            assert_eq!("2",                         result.graphic_info.workers[2].group);
            assert_eq!(0.75,                        result.graphic_info.workers[2].working_time.time);
            assert_eq!(None,                        result.graphic_info.workers[2].position);

            assert_eq!("ObiWanKenobi",              result.graphic_info.workers[3].name);
            assert_eq!("1",                         result.graphic_info.workers[3].group);
            assert_eq!(1.,                          result.graphic_info.workers[3].working_time.time);
            assert_eq!(Some("mieso".to_string()),   result.graphic_info.workers[3].position);

        assert_eq!(3,                               result.days.len());
            assert_eq!("20170101",                  result.days[0].date);
            assert_eq!("MagdalenaGryczka",          result.days[0].worker_name);
            assert_eq!("1",                           result.days[0].shift);
            assert_eq!("wydawka",                   result.days[0].position);

            assert_eq!("20170101",                  result.days[1].date);
            assert_eq!("MartynaGryczka",            result.days[1].worker_name);
            assert_eq!("2",                           result.days[1].shift);
            assert_eq!("salaA",                     result.days[1].position);

            assert_eq!("20170101",                  result.days[2].date);
            assert_eq!("ObiWanKenobi",              result.days[2].worker_name);
            assert_eq!("1",                           result.days[2].shift);
            assert_eq!("mieso",                     result.days[2].position);
    }
}