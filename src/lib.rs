#![feature(plugin, associated_consts)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
extern crate chrono;
extern crate rand;

pub mod routes;
pub mod global_variables;
pub mod errors_catchers;

pub mod graphic;
pub mod graphic_informations;
pub mod graphic_repository;
pub mod dto;
pub mod transactions;