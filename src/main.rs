#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate itertools;
extern crate rocket;
extern crate rocket_contrib;
extern crate rustc_serialize;
extern crate docopt;
extern crate serde_json;

extern crate graphic_generator;

use std::process;
use docopt::Docopt;

use graphic_generator::global_variables::{ExitCodes, APP_NAME, VERSION};
use graphic_generator::{errors_catchers, routes};


const USAGE: &'static str = "
graphic_generator.

Usage:
  graphic_generator
  graphic_generator (-h | --help)
  graphic_generator --version

Options:
  -h --help                             Show this screen
  --version                             Show version
";

#[derive(Debug, RustcDecodable)]
struct CmdArgs {
    flag_version: bool,
}

// --------------------------- main ---------------------------
fn main() {
    let cmd_args: CmdArgs = Docopt::new(USAGE)
        .and_then(|d| d.decode())
        .unwrap_or_else(|e| e.exit());

    if cmd_args.flag_version {
        display_version();
        process::exit(ExitCodes::OK);
    }

    rocket::ignite()
        .mount("/graphic_generator/", routes::create())
        .catch(errors_catchers::create())
        .launch();
}

fn display_version() {
    println!("{} v{}", APP_NAME, VERSION);
}
