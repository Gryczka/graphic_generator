use std;
use rocket::Route;
use rocket::Response;
use rocket::http::uri::URI;
use rocket_contrib::JSON;
use rocket::response::Responder;
use serde::Serialize;

use dto::ResponseTrait;
use dto::graphic::Graphic;
use graphic_informations::GraphicInformationsDoc;
// use dto::query_string::BookQueryParams;
use transactions::*;

pub fn create() -> Vec<Route> {
    routes![
        about,
        graphic_info,
        generate
    ]
}

macro_rules! try_get {
    ($expr:expr) => ( match $expr {
        Ok(val) => val,
        Err(err) => return std::convert::From::from(err)
    })
}

#[get("/about")]
fn about<'a, 'r>(uri: &'a URI) -> Response<'r> {
    response_builder(About::new(create()).execute())
}

#[post("/graphic_info", format = "application/json", data = "<json>")]
fn graphic_info<'a, 'r>(uri: &'a URI, json: JSON<GraphicInformationsDoc>) -> Response<'r> {
    response_builder(GraphicInfo::new(json).execute())
}

#[post("/generate", format = "application/json", data = "<json>")]
fn generate<'a, 'r>(uri: &'a URI, json: JSON<Graphic>) -> Response<'r> {
    response_builder(GenerateGraphic::new(json).execute())
}

fn response_builder<'a, 'r, R: ResponseTrait + Serialize >(response: R) -> Response<'r> {
    Response::build()
            .status(response.status())
            .merge(JSON(response).respond().unwrap())
            .finalize()
}