use global_variables::APP_NAME;
// use Error;
use dto;
use rocket::Route;
use std::env;
// use transactions::InfoTransaction;

const LOGNAME: &'static str = "LOGNAME";

#[derive(Debug)]
pub struct About{
    routes: Vec<Route>
}

impl About {
    pub fn new(routes: Vec<Route>) -> Self {
        About { routes }
    }

    pub fn execute(&self) -> dto::about::About {
        let mut dto_about   = dto::about::About::default();
        dto_about.app_name  = APP_NAME;
        dto_about.run_user  = env::var(LOGNAME)
            .or_else(|_| -> Result<String, String> { Ok("unknown".to_string()) })
            .expect("failed extracting LOGNAME from env::var");
        dto_about.available_transactions = self.routes
            .iter()
            .map(|route| {
                format!("{} {}", route.method.as_str(), route.path.path())
            })
            .collect();
        dto_about
    }
}
