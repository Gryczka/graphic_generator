use global_variables::DATA_FOLDER_PATH;
use rocket_contrib::JSON;
use graphic_repository::FileRepository;
use graphic::Graphic;
use dto::graphic::Graphic as dtoGraphic;
use graphic_informations::GraphicInformationsDoc;
use dto::day_item_container::DayContainer;

#[derive(Debug)]
pub struct GenerateGraphic {
    graphic: dtoGraphic,
}

impl GenerateGraphic {
    pub fn new(json: JSON<dtoGraphic>) -> Self {
        GenerateGraphic {
            graphic: json.into_inner(),
        }
    }

    pub fn execute(&self) -> Graphic {
        let file_repository = FileRepository::new(DATA_FOLDER_PATH, self.graphic.graphic_id.clone());
        let graphic_info = file_repository.load_graphic_info().unwrap();
        let generated_graphic = generate(graphic_info, &self.graphic.date);
        file_repository.save_graphic(&generated_graphic, &self.graphic.date).unwrap();
        generated_graphic
    }
}

fn generate(graphic_info: GraphicInformationsDoc, date: &String) -> Graphic {
    let mut graphic = Graphic::new(graphic_info);

    {
        let mut day_container = DayContainer::new(date, &graphic.graphic_info.shifts);

        for group in &graphic.graphic_info.groups {
            let workers = graphic.graphic_info.workers.iter().filter(|w| w.group == group.code).collect::<Vec<_>>();
            let positions = graphic.graphic_info.positions.iter().filter(|p| p.group == group.code).collect::<Vec<_>>();

            day_container.generate_graphic_for_one_group(&graphic.graphic_info, positions, workers);
        }

        graphic.days = day_container.get_days();

        // println!("DAY CONTAINER {:#?}", day_container);
    }

    // {
        // let group = &graphic.graphic_info.groups[0].code;

        // let workers = graphic.graphic_info.workers.iter().filter(|w| w.group == *group).collect::<Vec<_>>();

        // println!("WORKERS {:#?}", workers);
    // }

    graphic
}

#[cfg(test)]
mod tests {
    use super::*;
    use graphic_repository::Reader;
    use std::fs::File;

    #[test]
    fn test() {
        let target = Reader::new(File::open("tests/data/ID/ID.info").unwrap());
        let date = "201701".to_string();

        let result = target.read_graphic_informations_doc().expect("GraphicInformationsDoc");

        let graphic = generate(result, &date);

        assert!(false);
    }
}