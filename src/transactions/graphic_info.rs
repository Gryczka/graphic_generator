use global_variables::DATA_FOLDER_PATH;
use dto::graphic_information::GraphicInformationResponse;
use graphic_informations::GraphicInformationsDoc;
use rocket_contrib::JSON;
use graphic_repository::FileRepository;

#[derive(Debug)]
pub struct GraphicInfo {
    graphic_information: GraphicInformationsDoc
}

impl GraphicInfo {
    pub fn new(json: JSON<GraphicInformationsDoc>) -> Self {
        GraphicInfo {
            graphic_information: json.into_inner(),
        }
    }

    pub fn execute(&self) -> GraphicInformationResponse {
        let file_repository = FileRepository::new(DATA_FOLDER_PATH, self.graphic_information.graphic_info.graphic_id.clone());
        match file_repository.save_graphic_info(&self.graphic_information) {
            Ok(_)   => GraphicInformationResponse::new("Success".to_string()),
            Err(e)  => GraphicInformationResponse::new(e.to_string())
        }
    }
}